package echoclient;

/**
 *
 * Echo Client
 * v1.0
 * An application that connects to 'Echo Server' application. This application
 * will send strings based on user input and wait until the 'Echo Server' App to
 * return something. If the Server returns 'x', it will terminate the
 * application. This application able to calculate checksum that later will be
 * sent to the Server for data verification.
 * 
 * @author thomasdwidinata
 */
public class EchoClient {
    public static void main(String[] args) {
        // Check whether the argument given is 3, because the program receives 3 arguments, host address, port number, and checksum port
        if (args.length != 3) {
            System.err.println("Usage: java EchoClient <host name> <port number> <checksum port>");
            System.exit(1);
        }
        
        // Declare required variables
        String hostName = args[0];
        String data = new String(); // For saving the input from keyboard
        String serverResponse = new String(); // For saving the response from the Server
        int portNumber = Integer.parseInt(args[1]);
        int portNumberChecksum = Integer.parseInt(args[2]);
        
        // Connecting to the server by creating new instance of my class
        System.out.printf("Connecting to hostname '%s' with port '%s' and '%s' (Checksum Port)...\n", hostName, portNumber, portNumberChecksum);
        MySocketConnector socket = new MySocketConnector(hostName, portNumber, portNumberChecksum); // Our socket object, that has many API that can be called upon object creation
        
        // If socket is not connected, don't continue!
        if(!socket.isConnected())
        {
            System.err.printf("Unable to connect to the Server '%s':'%s' and checksum port '%s'\n", hostName, portNumber, portNumberChecksum);
            System.exit(1);
        }
        
        // If socket is connected to the server, continue!
        System.out.printf("Connected to '%s:%s' with checksum port '%s'!\n", hostName, portNumber, portNumberChecksum);
        
        while(data != null) // While server does not give any null data, the connection might be safe
        {
            System.out.printf("Enter a text: ");
            data = socket.readLine(); // Read from keyboard input
            socket.sendToServer(data); // Send the data to the server, with the checksum
            serverResponse = socket.getServerResponse(); // Wait until server gives response
            if(serverResponse == null) // If did not get any response, just notify the user
                System.out.println("Did not get any response from Server...");
            else if(serverResponse.equals("x")) // If server gives a character 'x', that means disconnection required
            {
                System.out.println("'x' character found! Exiting...");
                break;
            }
            else // Else, print the message from the Server
                System.out.printf("Echo from the Server : %s\n\n",serverResponse);
        }
        System.out.println("Closing Sockets...");
        socket.close(); // Close connection gracefully
    }
}
