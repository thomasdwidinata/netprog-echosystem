package echoclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

/**
 *
 * @author thomasdwidinata
 */
public class MySocketConnector {
    
    // Deckare required variables
    private Socket echoSocket;
    private Socket checksumSocket;
    private PrintWriter sender;
    private PrintWriter checksumSender;
    private BufferedReader receiver;
    private BufferedReader keyboardInput;
    private Checksum checksum;
    private String hostAddress;
    private int hostPort;
    private int checksumPort;
    
    /**
     * 
     * Constructor automatically connects to the Server. It defines the sockets,
     * checksum method, and buffers
     * 
     * @param hostName      = The server address to be connected
     * @param portNumber    = The server port for sending the data
     * @param checksumPort  = The server port for checksum receiving
     */
    public MySocketConnector(String hostName, int portNumber, int checksumPort)
    {
        try{
            // Set required information to connect to the server from the parameter
            this.hostAddress = hostName; 
            this.hostPort = portNumber;
            this.checksumPort = checksumPort;
            
            // Try to connect to the Server by creating new instance of Socket object
            echoSocket = new Socket(this.hostAddress, this.hostPort);
            echoSocket.setKeepAlive(true); // Enable always keep alive!
            checksumSocket = new Socket(this.hostAddress, this.checksumPort);
            checksumSocket.setKeepAlive(true);
            
            // Define checksum and initialise buffers
            checksum = new Adler32();
            sender = new PrintWriter(echoSocket.getOutputStream(), true);
            checksumSender = new PrintWriter(checksumSocket.getOutputStream(), true);
            receiver = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
            keyboardInput = new BufferedReader(new InputStreamReader(System.in));
        } catch (IOException ex) {
            Logger.getLogger(MySocketConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * Sends string to the connected Server on this instance.
     * 
     * @param data          = String that is going to be send to the Server
     */
    public void sendToServer(String data)
    {
        sender.println(data);
        checksumSender.println(this.checksum(data));
    }
    
    /**
     * 
     * Get server's response by waiting until the buffer gets end line
     * 
     * @return Wait the server response and returns string
     */
    public String getServerResponse()
    {
        try {
            return receiver.readLine();
        } catch (IOException ex) {
            Logger.getLogger(MySocketConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * 
     * Read keyboard input from the user (Client side)
     * 
     * @return Returns the keyboard input in String
     */
    public String readLine()
    {
        try{
            return keyboardInput.readLine();
        } catch (IOException ex) {
            Logger.getLogger(MySocketConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * 
     * Get the checksum file using Adler32 Algorithm on given string
     * 
     * @param data          = Receives string to be checksummed
     * @return Returns the Checksum in String instead of Long Int
     */
    public String checksum(String data)
    {
        checksum.reset();
        checksum.update(data.getBytes(), 0, data.getBytes().length);
        return String.valueOf(checksum.getValue());
    }
    
    /**
     * 
     * Get the echoSocket's connection
     * 
     * @return Returns the connection state
     */
    public boolean isConnected()
    {
        return this.echoSocket.isConnected();
    }
    
    /**
     * 
     * Close the connection gracefully
     * 
     */
    public void close()
    {
        try {
            echoSocket.close();
            checksumSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(MySocketConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
