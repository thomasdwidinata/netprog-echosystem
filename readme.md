# Network Progamming (COSC 1179)

## Lab Week 4-5 Exercise

### Description
The purpose of this exercise is to make a Java program that identify host computer's network adapter and show the IP Address and MAC address of it (Network Interface). The second one is to create a server application that able to serve client. The client will send a data to the Server and the server will echo the data back to the client (Basic Socket Communication).

#### First Task (Identify the Network Interface Programmatically)
In order to simplify the code, we use [Oracle's Java Tutorial](https://docs.oracle.com/javase/tutorial/networking/nifs/retrieving.html). The task is to identify the program of how the tutorial uses the classes. Then we modify the program in order to display additional information such as MAC Address in Hexadecimal form.

After running the code, all hostname of each interface will be different depending of the system. Some of the network interface does not have any MAC Address or network address.

#### Second and Third Task (Create Simple Server-Client Echo Application)
There are two program on this assignment, The `EchoServer` and `EchoClient`. The `EchoServer` is used for serving many clients. The `EchoServer` will automatically create new thread as more client connects to the Server. One thread will serve one client, but the thread shares the same Server System Socket, defined on the main of the program.

The `EchoClient` connects to the Server from specified Host and Ports. The `EchoClient` only sends data and checksum. The data and the checksum will have a different port to be connected.

### How to run

#### First Assignment
Go to `./SocketProgramming/dist/` folder, then run the `java` command:

`java -jar SocketProgramming.jar`

The program will launch and detect the network interfaces on the computer

#### Second and Third assignment
Question 2 and 3 are combined into 1 program.
To run `EchoServer`, simply go to `./EchoServer/dist/`. Then run this command to run the Server using port `8080` for data transportation and port `8081` for the checksum port:

`java -jar EchoServer.jar 8080 8081`

To run `EchoClient`, simply go to `./EchoClient/dist/`. Then run this command:

`java -jar EchoClient.jar <HOST_ADDRESS> <DATA_PORT> <CHECKSUM_PORT>`

The `<HOST_ADDRESS>` will be replaced with the Address of the server
The `<DATA_PORT>` will be replaced with the Port of the server (Data Port)
The `<CHECKSUM_PORT>` will be replaced with the Port of the server Checksum
