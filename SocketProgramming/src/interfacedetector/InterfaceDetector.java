package interfacedetector;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author thomasdwidinata
 */
public class InterfaceDetector {
    public static void main(String[] args) {
        byte[] macCache;
        String mac = "";
        String cache = "";
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            Enumeration<InetAddress> addresses;
            for(NetworkInterface networkInterface : Collections.list(networkInterfaces))
            {
                addresses = networkInterface.getInetAddresses();
                mac = new String();
                System.out.println("Adapter Interface\t: " + networkInterface.getDisplayName());
                System.out.println("Adapter Name\t\t: " + networkInterface.getName());
                macCache = networkInterface.getHardwareAddress();
                if(macCache != null){
                    mac = DatatypeConverter.printHexBinary(macCache);
                    cache = "";
                    for(int i = 0; i < mac.length(); i+=2){
                        cache += mac.substring(i, i+2);
                        if(i < (mac.length())-2)
                            cache += ":";
                    }
                    mac = cache;
                }
                System.out.println("Adapter MAC Addr\t: " + mac);
                for(InetAddress address : Collections.list(addresses))
                {
                    System.out.println("Adapter Address\t\t: " + address.getHostAddress());
                    System.out.println("Hostname \t\t: " + address.getCanonicalHostName());
                }
                displaySubInterface(networkInterface);
                System.out.println("\n");
            }
        } catch (SocketException ex) {
            Logger.getLogger(InterfaceDetector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void displaySubInterface(NetworkInterface interfaces)
    {
        Enumeration<NetworkInterface> subInts = interfaces.getSubInterfaces();
        for(NetworkInterface subInterface : Collections.list(subInts))
        {
            System.out.println("Sub Interface Display Name\t: " + subInterface.getDisplayName());
            System.out.println("Sub Interface Name\t: " + subInterface.getName());
        }
    }
    
}
