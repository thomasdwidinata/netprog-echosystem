package echoserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.BindException;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

/**
 *
 * The Server Thread, used for serving the clients. One thread one client!
 * Must be managed by the main class in order to start the thread!
 * 
 * @author thomasdwidinata
 */
public class ServerThread implements Runnable{
    
    // Define required variables
    private PrintWriter printer;
    private BufferedReader reader;
    private BufferedReader checksumReader;
    private Socket clientSocket;
    private Socket checksumSocket;
    private Checksum checksum;
    private String inputLine;
    private String cache;
    
    public ServerThread(Socket clientSocket, Socket checksumSocket)
    {
        // Set client socket for current Thread
        this.clientSocket = clientSocket;
        this.checksumSocket = checksumSocket;
        this.checksum = new Adler32(); // Use Adler32 as Checksum Algorithm
        this.inputLine = new String();
    }

    @Override
    public void run() {
        System.out.printf("Incoming Client! Thread is now running. A client from '%s' with Hostname '%s'\n", clientSocket.getRemoteSocketAddress(), clientSocket.getInetAddress().getCanonicalHostName());
        try{
            // Get the client's socket input stream and output stream
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            printer = new PrintWriter(clientSocket.getOutputStream(), true);
            
            // Get the client's checksum socket input stream for read only mode
            checksumReader = new BufferedReader(new InputStreamReader(checksumSocket.getInputStream()));
            while((inputLine = reader.readLine()) != null) // always read the connection until '\n' received
            {
                cache = checksumReader.readLine(); // Get the checksum, because checksum always sent after the text given
                System.out.printf("Got message from '%s' = '%s'\n", clientSocket.getRemoteSocketAddress(), inputLine);
                
                // Reset the Checksum Object and try to checksum the string by converting it into Bytes
                checksum.reset();
                checksum.update(inputLine.getBytes(), 0, inputLine.getBytes().length);
                
                // If the checksum is null, possible broken data received. If not, compare checksum
                if(cache == null)
                    System.out.printf("null checksum received... Possible of broken data!\n");
                else
                {
                    if(cache.equals(String.valueOf(checksum.getValue())))
                        System.out.printf("Checksum Match! Checksum received : %s\n", cache);
                    else
                        System.out.printf("Checksum not match! Received Checksum: %s ; Data checksumed on the Server : %s\n", cache, checksum.getValue());
                }
                printer.println(inputLine); // Resend the data to Client again...
            }
            // If the reader buffer is null, then it is possible that the connection is broken.. Then close the connection and terminate the Thread to give more memory space to the System
            System.out.printf("Client '%s' has left.\n", clientSocket.getRemoteSocketAddress());
            // Gracefully close the connection
            printer.close(); 
            reader.close();
            clientSocket.close();
            checksumReader.close();
            checksumSocket.close();
        } catch (SocketException ex){ // If SocketException is raised, connection is not stable and might be lost...
            System.err.printf("Connection to the client (%s) has lost!\n", clientSocket.getRemoteSocketAddress());
        }
        catch (IOException ex) {
            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Terminate the thread because client no longer connects
        System.out.printf("Closing Thread for '%s'\n\n", clientSocket.getRemoteSocketAddress());
        Thread.currentThread().interrupt();
    }
}
