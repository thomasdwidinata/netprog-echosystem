package echoserver;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Echo Server
 * v1.0
 * 
 * A Server application that serves many clients by creating new thread each
 * time new client connects to the Server. The server only echoes what the
 * client sends. The server also serves a socket that receives checksum that
 * the client send to ensure that the data received by the server is not broken.
 * 
 * @author thomasdwidinata
 */
public class EchoServer {
    public static void main(String[] args) {
        // Detect whether required argument is given or not
        if (args.length != 2) {
            System.err.println("Usage: java EchoServer <port number> <checksum port>");
            System.exit(1);
        }
        System.out.printf("Starting Server... Will be using Port '%s' with checksum port '%s'\n", args[0], args[1]);
        
        // Initialise required variables
        int portNumber = Integer.valueOf(args[0]);
        int portNumberChecksum = Integer.valueOf(args[1]);
        ServerSocket serverSocket = null;
        ServerSocket checksumSocket = null;
        Socket clientSocket;
        Socket checksumClientSocket;
        ArrayList<Socket> clients = new ArrayList<>(); // Saving many Client Sockets, in array...
        ArrayList<Socket> checksumClient = new ArrayList<>();
        
        try{
            // Initialise the Server Socket and immediately start it
            serverSocket = new ServerSocket(portNumber); // Reserve the port number for Server
            checksumSocket = new ServerSocket(portNumberChecksum); // Reserve the port number for Checksum purpose
            
            // Announcing that the server has started
            System.out.printf("Server started at '%s'\n", serverSocket.getLocalSocketAddress());
            
            // Wait for incoming clients
            while((clientSocket = serverSocket.accept()) != null){
                checksumClientSocket = checksumSocket.accept();
                clientSocket.setKeepAlive(true);
                checksumClientSocket.setKeepAlive(true);
                new Thread( new ServerThread(clientSocket, checksumClientSocket) ).start(); // Start new thread for connected client 
            }
        }
        catch(BindException ex){
            System.err.printf("Unable to start server! Port %s or %s already in use!\n", portNumber, portNumberChecksum);
        } 
        catch(IOException ex){
            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}